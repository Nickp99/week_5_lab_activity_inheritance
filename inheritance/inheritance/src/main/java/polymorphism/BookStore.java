package polymorphism;

public class BookStore {
    public static void main(String[] args){
        Book book[]= new Book[5];

        book[0]= new Book("Artemis Fowl The lost Colony","Eoin Colfer");
        book[1]= new ElectronicBook("no idea the ebook","Riordan",64);
        book[2]= new Book("Artemis Fowl The Opal Deception","Eoin Colfer");
        book[3]= new ElectronicBook("Hockey the ebook","Crosby",274);
        book[4]= new ElectronicBook("City of Bones","Clare",953);

        for(int i =0; i< book.length;i++){
            System.out.println(book[i].toString());
        }
    }
}
