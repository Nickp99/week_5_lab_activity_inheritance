package polymorphism;

public class ElectronicBook extends Book{
    private int numberBytes;
    public ElectronicBook(String title,String author, int number){
        super(title, author);
         this.numberBytes=number;
    }

    public int getNumberBytes(){
        return this.numberBytes;
    }
    public String toString(){
        String base=super.toString();
        return base +" size: "+this.numberBytes;
    }
}


