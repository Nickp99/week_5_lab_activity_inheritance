package polymorphism;
import static org.junit.Assert.*;

import org.junit.Test;


public class ElectronicBookTEST {

    private ElectronicBook ebook;
    @Test
    public void testgetTitle(){
        ebook = new ElectronicBook("title", "author",5);
        assertEquals( "makeing sure its title","title", ebook.getTitle());
        
    }
    @Test
     public void testgetAuthor(){
         ebook = new ElectronicBook("title", "author",5);
        assertEquals( "makeing sure its author","author", ebook.getAuthor());
    }
     @Test
     public void testgetSize(){
         ebook = new ElectronicBook("title", "author",5);
        assertEquals( "makeing sure its 5",5, ebook.getNumberBytes());
    }
    @Test
    public void testtoString(){
         ebook = new ElectronicBook("title", "author",5);
        assertEquals( "makeing sure its true","title by author size: 5", ebook.toString());
    }
}
