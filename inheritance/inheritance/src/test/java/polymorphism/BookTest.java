package polymorphism;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class BookTest 
{
    /**
     * Rigorous Test :-)
     * 
     */
    private Book book;
    @Test
    public void testgetTitle(){
        book = new Book("title", "author");
        assertEquals( "makeing sure its true","title", book.getTitle());
        
    }
    @Test
     public void testgetAuthor(){
         book = new Book("title", "author");
        assertEquals( "makeing sure its true","author", book.getAuthor());
    }
    @Test
    public void testtoString(){
         book = new Book("title", "author");
        assertEquals( "makeing sure its true","title by author", book.toString());
    }
}
